function getCello
	git clone https://github.com/orangeduck/Cello
cd Cello
make check
cp libCello.a ../
cp include/Cello.h ../
cd ..
rm -rf Cello
end
